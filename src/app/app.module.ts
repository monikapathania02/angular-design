import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MarketplaceModule } from './marketplace/marketplace.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    MarketplaceModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
