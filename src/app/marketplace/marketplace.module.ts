import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropetiesComponent } from './propeties/propeties.component';
import { SharedModule } from '../shared/shared.module';




@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [    
    PropetiesComponent
  ],
  declarations: [
    PropetiesComponent
  ],
})
export class MarketplaceModule { }
